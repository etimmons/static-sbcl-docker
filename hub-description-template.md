- [Supported Tags](#orgd666fda)
  - [Simple Tags](#org0e0a2da)
  - [Shared Tags](#org4ec710d)
- [Quick Reference](#org91cbd49)
- [What is SBCL?](#org5e04fdd)
- [How to use this image](#orged802bb)
  - [Create a `Dockerfile` in your SBCL project](#org475131a)
  - [Run a single Common Lisp script](#orgcd77f85)
  - [Developing using SLIME](#org8d99a23)
- [What's in the image?](#org126245e)
- [License](#orga51012f)



<a id="orgd666fda"></a>

# Supported Tags


<a id="org0e0a2da"></a>

## Simple Tags

INSERT-SIMPLE-TAGS


<a id="org4ec710d"></a>

## Shared Tags

INSERT-SHARED-TAGS


<a id="org91cbd49"></a>

# Quick Reference

-   **SBCL Home Page:** [http://sbcl.org](http://sbcl.org)
-   **Where to file Docker image related issues:** <https://gitlab.common-lisp.net/etimmons/static-sbcl-docker>
-   **Where to file issues for SBCL itself:** [https://bugs.launchpad.net/sbcl](https://bugs.launchpad.net/sbcl)
-   **Maintained by:** Eric Timmons
-   **Supported platforms:** `linux/amd64`, `linux/arm64/v8`, `linux/arm/v7`


<a id="org5e04fdd"></a>

# What is SBCL?

From [SBCL's Home Page](http://sbcl.org):

> Steel Bank Common Lisp (SBCL) is a high performance Common Lisp compiler. It is open source / free software, with a permissive license. In addition to the compiler and runtime system for ANSI Common Lisp, it provides an interactive environment including a debugger, a statistical profiler, a code coverage tool, and many other extensions.


<a id="orged802bb"></a>

# How to use this image


<a id="org475131a"></a>

## Create a `Dockerfile` in your SBCL project

```dockerfile
FROM %%IMAGE%%:latest
COPY . /usr/src/app
WORKDIR /usr/src/app
CMD [ "sbcl", "--load", "./your-daemon-or-script.lisp" ]
```

You can then build and run the Docker image:

```console
$ docker build -t my-sbcl-app
$ docker run -it --rm --name my-running-app my-sbcl-app
```


<a id="orgcd77f85"></a>

## Run a single Common Lisp script

For many simple, single file projects, you may find it inconvenient to write a complete \`Dockerfile\`. In such cases, you can run a Lisp script by using the SBCL Docker image directly:

```console
$ docker run -it --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app %%IMAGE%%:latest sbcl --load ./your-daemon-or-script.lisp
```


<a id="org8d99a23"></a>

## Developing using SLIME

[SLIME](https://common-lisp.net/project/slime/) provides a convenient and fun environment for hacking on Common Lisp. To develop using SLIME, first start the Swank server in a container:

```console
$ docker run -it --rm --name sbcl-slime -p 127.0.0.1:4005:4005 -v /path/to/slime:/usr/src/slime -v "$PWD":/usr/src/app -w /usr/src/app %%IMAGE%%:latest sbcl --load /usr/src/slime/swank-loader.lisp --eval '(swank-loader:init)' --eval '(swank:create-server :dont-close t :interface "0.0.0.0")'
```

Then, in an Emacs instance with slime loaded, type:

```emacs
M-x slime-connect RET RET RET
```


<a id="org126245e"></a>

# What's in the image?

This image contains SBCL binaries built with &#x2013;with-sb-linkable-runtime and &#x2013;sn-prelink-linkage-table. The latter is provided by a series of patches described at <https://www.timmons.dev/posts/static-executables-with-sbcl-v2.html>. The goal is to get these patches (or equivalent) merged upstream and then stop supporting these images.


<a id="orga51012f"></a>

# License

SBCL is licensed using a mix of BSD-style and public domain licenses. See SBCL's [COPYING](http://sbcl.git.sourceforge.net/git/gitweb.cgi?p=sbcl/sbcl.git;a=blob_plain;f=COPYING;hb=HEAD) file for more info.

The Dockerfiles used to build the images are licensed under BSD-2-Clause.

As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.
