#+TITLE: Static SBCL Docker Images

This project contains Docker images to build SBCL with static executable
support and the infrastructure to build the images.
